## Include Full Calendar

You can always [check the instructions](https://fullcalendar.io/docs/usage/) on full calendar's home page. Along with its own files, you will also need jQuery and Moment.js.

To include its base css, I used `fullcalendar.scss` inside the `_sass` folder and I imported it inside `assets/main.scss`.

You will also need to include jQuery, Moment and `fullcalender.js` in your page. Check the bottom of `_layouts/default.html` in the source code.



##  Global configuration

### Set a default year and month

Full calendar provides ,well, a calendar. Since we needed it to show just a date-independent week, I had to tell it to show a particular and arbitrary week of the year. This means that we have to have a fixed value for the year, the month and the starting date of the week. For the first two, in **`config.yml`**, I added the following lines:

```yaml
# Default arbitary values used in the static calendar.
# Please do not edit!
default_year: 2017
default_month: 11 
```

Just add them and leave them there.



### Add select options for Cloudcannon

In order to have a select list field in cloudcannon, you need to somehow define its options. You can do this inside `_config.yml`.

For the names of the events, add the following:

```yaml
event_names:
  - CrossFit
  - Haven Fit
  - Community Class
  - CFSH Strength
  - Open Gym 
```

Cloudcannon will see this list and will associate a field called `event_name`.

For the tags, you can add the following:

```yaml
event_tags:
  - red
  - blue
  - green
  - coral
  - purple  
```

You will also need to add the days of the week, like so:

```yaml
event_days:
  - Monday
  - Tuesday
  - Wednesday
  - Thursday
  - Friday
  - Saturday
  - Sunday
```

Now we have three fields available: `event_name`, `event_tag` and `event_day`. 



## Event Colors

So far you have entered the array for `event_tags` in `_config.yml`. You can map the color names with the actual colors to be used, like so:

```yaml
event_colors:
  green: '#9EF3A7'
  blue: '#84B8DC'
  red: '#ed888a'
  coral: '#FAB79D'
  purple: '#E4BAFA' 
```

If you choose a value of `green`, the event on the calendar will have a background of the color `#9EF3A7`. You can adjust the arrays `event_tags` and `event_colors` to your own needs.



## The calendar page

This is the page that displays the calendar. I named the file `calendar.html` and contains the following code:

```yaml
---
layout: default
---

<div id='calendar'></div>
```

You can define the layout there. I used the default one. This page has a permalink of `/calendar`. If you want a different one, just give the file another name or use the `permalink` front matter field.



## Calendar initialization

The calendar gets initializes with the following js code:

```javascript
$(document).ready(function() {
    
    // Initialize the calendar
    $('#calendar').fullCalendar({
        
        header: false, // remove the header
        allDaySlot: false, // remove the allday row
        slotLabelInterval: 30, // show the hours in 30 minutes intervals
        firstDay: 1, // make Monday the first day
        columnFormat: 'dddd', // display full day names
        minTime: '06:00:00', // start from 6:00 am
        maxTime: '20:30:00', // finish at 8:30 pm
        defaultDate: '2017-11-20', // arbitary default date. The calendar shows the week of this date.        
        defaultView: 'agendaWeek', // Use full calendar's agendaWeek view
        events: '/calendar-data' // the URL to get the data from

    });

});
```

You can include it either in html inside a `<script>` tag or in your custom `.js` file. Just make sure that the code is wrapped inside a `$(document).ready(function()` function and that is executed after the code that includes the library files.

The file I used, is called `fullcalendar-init.js`.



## Formatting the data

Notice this setting from above: `events:'/calendar-data'`. It tells full calendar to grab its data from a page with a permalink of `/calendar-data`. In this file, we will construct the needed data in `json` format. The file is named **`calendar.data.html`**. Just place it in your Jekyll's root folder and it will just work. Of course, you can check its code to see how it works.

## The Data

The data for the calendar is stored inside the `_data/events.yml` file. If you don't have a `_data` folder, just create it manually. The code for a couple of events, can be the following:

```yaml
- event_name: CrossFit
  event_day: Monday
  event_start_time: '6:00 am'
  event_end_time: '7:00 am'
  event_tag: green
  
- event_name: CrossFit
  event_day: Monday
  event_start_time: '12:00 pm'
  event_end_time: '13:00 pm'
  event_tag: green
```

**Make sure that `_data/events.yml` already has data for at least one event**, so that Cloudannon will know which fields to use.

## Raw Data

If you want to check the raw `json` that gets generated, just visit `/calendar-data`.

## Data Input

There are two ways to enter data for the calendar; Directly, in `_data/events.yml` or in Cloudcannon's GUI. Please bare in mind that in `_data/events.yml` the values for the time fields need to be inside quotes and if you have single digit hours, you must use just one digit. For instance, you must enter `'8:00 am'`  and NOT `'08:00 am'`.

Please bare in mind that the format for the time follows the 12 hour pattern. So you must input `'1:00 pm'` and NOT `'13:00 pm'` or `'13:00'`.

The Cloudcannon GUI resides in Collections -> Data -> Events

## Hiding info with CSS

Full calendar highlights the current day, so if you want to hide the highlight, just add to your CSS:

```css
.fc-today {
	background-color: initial !important;
}
```

Also, along with the event name Full calendar displays the hours as well. If you do not want that, just add:

```css
.fc-content .fc-time{
  display : none !important;
}
```

