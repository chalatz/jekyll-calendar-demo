$(document).ready(function() {
    
    // Initialize the calendar
    $('#calendar').fullCalendar({
        
        header: false, // remove the header
        allDaySlot: false, // remove the allday row
        slotLabelInterval: 30, // show the hours in 30 minutes intervals
        firstDay: 1, // make Monday the first day
        columnFormat: 'dddd', // display full day names
        minTime: '06:00:00', // start from 6:00 am
        maxTime: '20:30:00', // finish at 8:30 pm
        defaultDate: '2017-11-20', // arbitary default date. The calendar shows the week of this date.        
        defaultView: 'agendaWeek', // Use full calendar's agendaWeek view
        events: '/calendar-data' // the URL to get the data from

    });

});